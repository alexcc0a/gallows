package com.nesterov;


import java.util.Scanner;

// Объявление приватных статических констант `MAX_MISSES` со значением 6 и
// `HANGMAN_ASCII` - массива строк, представляющих ASCII-графику для отображения
// состояний виселицы в игре.
public class Main {
    private static final int MAX_MISSES = 6;
    private static final String[] HANGMAN_ASCII = {
            "   +---+\n" +
                    "   |   |\n" +
                    "       |\n" +
                    "       |\n" +
                    "       |\n" +
                    "       |\n" +
                    "=========",
            "   +---+\n" +
                    "   |   |\n" +
                    "   O   |\n" +
                    "       |\n" +
                    "       |\n" +
                    "       |\n" +
                    "=========",
            "   +---+\n" +
                    "   |   |\n" +
                    "   O   |\n" +
                    "   |   |\n" +
                    "       |\n" +
                    "       |\n" +
                    "=========",
            "   +---+\n" +
                    "   |   |\n" +
                    "   O   |\n" +
                    "  /|   |\n" +
                    "       |\n" +
                    "       |\n" +
                    "=========",
            "   +---+\n" +
                    "   |   |\n" +
                    "   O   |\n" +
                    "  /|\\  |\n" +
                    "       |\n" +
                    "       |\n" +
                    "=========",
            "   +---+\n" +
                    "   |   |\n" +
                    "   O   |\n" +
                    "  /|\\  |\n" +
                    "  /    |\n" +
                    "       |\n" +
                    "=========",
            "   +---+\n" +
                    "   |   |\n" +
                    "   O   |\n" +
                    "  /|\\  |\n" +
                    "  / \\  |\n" +
                    "       |\n" +
                    "========="
    };

    public static void main(String[] args) {
        // Инициализация переменной `word` со значением "halloween",
        // массива `guessedLetters` с длиной, равной длине слова `word`,
        // и переменной `misses` с начальным значением 0.
        String word = "halloween";
        char[] guessedLetters = new char[word.length()];
        int misses = 0;

        // Цикл `for`, который присваивает каждому элементу массива `guessedLetters`
        // символ '*' для представления угаданных и неугаданных букв.
        for (int i = 0; i < word.length(); i++) {
            guessedLetters[i] = '*';
        }

        Scanner scanner = new Scanner(System.in); // Создание объекта `Scanner` для чтения пользовательского ввода.
        System.out.println("Впервые как полноценный обычай оно было зафиксировано " +
                "\nв начале XX века и восходит к американским традициям " +
                "\nкостюмированных вечеринок." +
                "\nО каком прозднике идет речь? (Слово на английском)");

        // Бесконечный цикл `while`, который будет выполняться,
        // пока не будет достигнуто условие выхода.
        while (true) {
            printHangman(misses);
            // Вызов метода `printHangman` с аргументом `misses`.
            System.out.println("Угаданное слово: " + String.valueOf(guessedLetters));
            System.out.print("Введите букву: ");
            // Считывание символа из пользовательского ввода и приведение его к нижнему регистру.
            char guess = scanner.nextLine().toLowerCase().charAt(0);

            // Проверка, не была ли уже угадана эта буква.
            if (!checkIfLetterAlreadyGuessed(guess, guessedLetters)) {
                System.out.println("Вы уже угадали эту букву!");
                continue;
            }

            // Объявление переменной `foundLetter` со значением `false` для отслеживания,
            // была ли найдена угаданная буква в слове.
            boolean foundLetter = false;
            // Цикл `for`, который перебирает каждую букву в слове и проверяет,
            // совпадает ли она с угаданной буквой.
            for (int i = 0; i < word.length(); i++) {
                // Если буква совпадает, то она записывается в массив `guessedLetters`
                // в соответствующую позицию и переменная `foundLetter` становится `true`.
                if (word.charAt(i) == guess) {
                    guessedLetters[i] = guess;
                    foundLetter = true;
                }
            }

            // Если буква не была найдена в слове, то переменная `misses` увеличивается на 1.
            if (!foundLetter) {
                misses++;
            }

            if (misses == MAX_MISSES) {
                printHangman(misses);
                System.out.println("У вас закончились попытки! Человечка спасти не удалось :(");
                System.out.println("Загаданное слово: " + word);
                break;
            }

            if (String.valueOf(guessedLetters).equals(word)) {
                System.out.println("Поздравляем! Вы угадали слово!");
                System.out.println("Загаданное слово: " + word);
                break;
            }
        }
    }

    // Метод, который проверяет, была ли уже угадана указанная буква.
    private static boolean checkIfLetterAlreadyGuessed(char guess, char[] guessedLetters) {
        for (char letter : guessedLetters) {
            if (letter == guess) {
                return false;
            }
        }
        return true;
    }

    // Метод, который выводит на консоль текущее состояние виселицы в зависимости от количества ошибок.
    private static void printHangman(int misses) {
        System.out.println(HANGMAN_ASCII[misses]);
    }
}
